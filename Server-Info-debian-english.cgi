#!/usr/bin/perl

# Server-Info.cgi : CGI que permite visualizar mediante el navegador informacion de tu sistema
# =~  is a special operator that invokes the regular expression engine. 
# Podemos ver las variables de entorno usadas por perl:
#   foreach $key (sort keys(%ENV)) {
#      print "$key = $ENV{$key}<p>";
#   }


# GMT a local
use Time::Local;
#use strict;
#use CGI ':standard';
#use Net::Ifconfig::Wrapper; # deber estar instalada la libreria libnet-ip-perl y libnet-ifconfig-wrapper-perl

# --------------- Definicion de variables -----------------#

$ENV{PATH} = "/sbin:/usr/local/bin:/usr/bin:/bin"; # Definimos las rutas de los ejecutables, por defecto: /usr/local/bin:/usr/bin:/bin
my $refresco_html = '';  ## indica el numero de segundos para la variable html meta refresh, si dejamos vacio este valor no se refrescara la pagina
my $color_fondo='white';
my $color_cabecera = 'green';
my $color_texto = 'black';
my $color_link = 'blue';
my $vcolor_link = 'purple';
my $color_aviso = 'orange';
my $color_peligro = 'red';
# my $fuente = 'Clean, Fixed, Courier New, Courier, Terminal, Screen';
my $fuente = 'Arial, Helvetica, Geneva';
my $tam_fuente='-1';

my ($c_segundo,$c_minuto,$c_hora,$c_dia,$c_mes,$c_ano,$c_wdia,$c_ydia,$c_isdst) = localtime(time);
$c_ano = 1900 + $c_ano;
if ($c_minuto < 10) {
  $c_minuto = "0$c_minuto";
}

# Si hemos definido la variable insertamos el tag correspondiente para realizar el refresco de la pagina html
if ($refresco_html) {
  $tag_refresco = "<META HTTP-EQUIV=Refresh CONTENT=$refresco_html>";
}



# --------------- Inicio de la pagina -----------------#

print "Content-type: text/html; charset=UTF-8\n\n";
print qq{
<HTML>
<HEAD>
<TITLE> System information - $ENV{SERVER_NAME} </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
$tag_refresco
</HEAD>
<BODY BGCOLOR=$color_fondo TEXT=$color_texto LINK=$color_link VLINK=$vcolor_link>
<FONT FACE="$fuente" SIZE="$tam_fuente">

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#F8F8F8">
<tr>
   <td>
   <center>
   <font color="#5A6770" face="$fuente" size=5>
	<b>Server information: $ENV{SERVER_NAME} - Date: $c_ano-$c_mes-$c_dia - $c_hora:$c_minuto</b>
   </font>
   </center>
   </td>
</tr>
<tr>
   <td bgcolor="#EEEEEE">
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     System admins control and administration information
     </font>
     </center>
     </td>
</tr>
</table>
</center>
   
   
<br><br>

<FONT size=3 COLOR=#5A6770>General system information:</FONT>
<ol>
<li><a href=#uptime style="color:red; text-decoration:none";>uptime - Tell how long the system has been running</a></li>
<li><a href=#free style="color:red; text-decoration:none";>free - Display amount of free and used memory in the system</a></li>
<li><a href=#who style="color:red; text-decoration:none";>who - show who is logged on</a></li>
<li><a href=#tail style="color:red; text-decoration:none";>tail -100 \$HOME/.bash_history - Last 100 commands executed</a></li>
<li><a href=#last style="color:red; text-decoration:none";>last - show listing of last logged in users</a></li>
<li><a href=#grub style="color:red; text-decoration:none";>grub - GRand Unified Bootloader</a></li>
<li><a href=#rc3 style="color:red; text-decoration:none";>rc3 startup services</a></li>
<li><a href=#shells style="color:red; text-decoration:none";>Active shells in the system</a></li>
</ol>

<br>
<FONT size=3 COLOR=#5A6770>File system information:</FONT>
<ol> 
<li><a href=#df style="color:red; text-decoration:none";>df -h - report file system disk space usage</a></li>
<li><a href=#dfi style="color:red; text-decoration:none";>df -i - report file system inodes usage</a></li>
<li><a href=#setuid style="color:red; text-decoration:none";>find / -type f -perm -4000 -print 2>/dev/null - active setuid files</a></li>
<li><a href=#setuid style="color:red; text-decoration:none";>find / -type f -perm -2000 -print 2>/dev/null - active setgid files</a></li>
<li><a href=#rhost style="color:red; text-decoration:none";>find / -type f -name .rhosts -print 2>/dev/null - .rhosts files</a></li>
<li><a href=#ficheroescritura style="color:red; text-decoration:none";>find / -type f -perm -2 -print 2>/dev/null - files with write permission for all users</a></li>
<li><a href=#ficheros777 style="color:red; text-decoration:none";>find / -type f -perm 777 - files with 777 permissions</a></li>
<li><a href=#dir777 style="color:red; text-decoration:none";>find / -type d -perm 777 - directories with 777 permissions</a></li>
<li><a href=#runFilesNoGroup style="color:red; text-decoration:none";>find /  -nouser -o -nogroup - files without gruop</a></li>
<li><a href=#tmp style="color:red; text-decoration:none";>/tmp content</a></li>
</ol>

<br>
<FONT size=3 COLOR=#5A6770>TCP/IP Information:</FONT>
<ol> 
<li><a href=#nmap style="color:red; text-decoration:none";>nmap - Network exploration tool and security / port scanner</a></li>
<li><a href=#rpc style="color:red; text-decoration:none";>rpcinfo -p localhost - show the RPC services registered on the local machine use</a></li>
<li><a href=#rutas style="color:red; text-decoration:none";>netstat -nr - displays the contents of the routing  tables</a></li>
<li><a href=#activeconections style="color:red; text-decoration:none";>netstat -ta - Active Internet connections (servers and established)</a></li>
<li><a href=#ifconfig style="color:red; text-decoration:none";>ifconfig -a - Configure a network interface and displays the status of the currently active interfaces</a></li>
</ol>

<br>
<FONT size=3 COLOR=#5A6770>Processes and programs running in the system:</FONT>
<ol>  
<li><a href=#ps style="color:red; text-decoration:none";>ps - report a snapshot of the current processes</a></li>
<li><a href=#rpms style="color:red; text-decoration:none";>dpkg --get-selections - installed packages</a></li>
<li><a href=#top style="color:red; text-decoration:none";>top - display Linux tasks</a></li>
</ol> 

<br>
<FONT size=3 COLOR=#5A6770>Security information:</FONT>
<ol>  
<li><a href=#compruebaShells style="color:red; text-decoration:none";>Check the file /etc/shells</a></li>
<li><a href=#checkNoRootSSH style="color:red; text-decoration:none";>Security check for the sshd server</a></li>
<li><a href=#checkDisabledCtrlAltDel style="color:red; text-decoration:none";>Check disable of CtrlAltDel</a></li>
<li><a href=#checkCrontab style="color:red; text-decoration:none";>Security check for the crontab</a></li>
<li><a href=#checkApache style="color:red; text-decoration:none";>Configuration check for apache server</a></li>
<li><a href=#recomendaciones style="color:red; text-decoration:none";>Security recomendations</a></li>
</ol> 

<br><br>
};

print qq{
<hr color=#AB1517>  <!-- línea de separación roja -->
};



# --------- En este punto llamamos a las funciones para generar la informacion -----------#


print qq{
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     General system information
     </font>
     </center>
     </td>
</tr>
</table>
</center>
};


&uptime;
&espacio_libre;
&usuarios;
&tail;
&last;
&grub;
&rc3;
&runShells;




print qq{
<br><br><br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     File system information
     </font>
     </center>
     </td>
</tr>
</table>
</center>

};

&espacio_disco;
&espacio_inodos;
&setuid;
&setgid;
&rhosts;
&ficheroescritura;
&ficheros777;
&dir777;
&runFilesNoGroup;
&tmp;


print qq{
<br><br><br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     TCP/IP Information
     </font>
     </center>
     </td>
</tr>
</table>
</center>
};

&nmap;
&rpcinfo;
&rutas;
&activeconections;
&IP;





print qq{
<br><br><br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     Processes and programs running in the system
     </font>
     </center>
     </td>
</tr>
</table>
</center>

};

&procesos;
&rpms;
&top;



print qq{
<br><br><br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     Security information
     </font>
     </center>
     </td>
</tr>
</table>
</center>

};

&compruebaShells;
&checkNoRootSSH;
&checkDisabledCtrlAltDel;
&checkCrontab;
&checkApache;
&recomendaciones;



# --------- Fin llamadas a funciones -----------#


print qq{
<br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
</FONT>
</BODY>
</HTML>
};



# --------- final de la parte principal -----------#






# --------- Definicion de funciones -----------#

# Funcion que define el tiempo que lleva el sistema levantado
sub uptime {

	chop($uptime = `uptime`); # Eliminamos el retorno de carro
	$uptime =~ s/\t/     /g;
	$uptime =~ s/\s/\&nbsp\;/g;

	print qq{
		<a name=uptime></a>
		<br><br><br>
		<FONT COLOR=$color_cabecera>uptime gives a one line display of the following information.  The current time, how long the system has been running, how many users are currently logged on, and the
       system load averages for the past 1, 5, and 15 minutes: </FONT>uptime
		<br><br>
		<FONT COLOR=$color_cabecera>Uptime: </FONT> $uptime
		<br>
	};

} # Final de la funcion uptime


# Funcion que visualiza la memoria libre y usuada del sistema
sub espacio_libre {

	@free = `free -o`;
	$informacion = shift @free; 
	$informacion = shift @free;
	
	my ($tipo, $total, $usado, $libre, $compartido, $buffers, $cached) = split(/\s+/, $informacion); # separamos en variables los datos obtenidos (separados por espacios)

	$capacidad = (($usado/$total) * 100);
	if ($capacidad < 50) {
		$color = $color_texto;
	} elsif ($capacity < 90) {
		$color = $color_aviso;
	} else {
		$color = $color_peligro;
	}
	print "<a name=free></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>free displays the total amount of free and used physical and swap memory in the system, as well as the buffers used by the kernel:</FONT> free -o";
	print "<br><br>\n";	
	print "<FONT COLOR=$color_cabecera>RAM memory used: </FONT><FONT COLOR=$color>";
	
	$capacidad = int($capacidad);
	$capacidad =~ s/$capacidad/$capacidad%/;
	print ("Used memory: ", $capacidad);
	
	print qq{ </FONT> &nbsp; &nbsp;--->  &nbsp; &nbsp;  Total Memory: $total Kbytes  --  Free Memory: $libre Kbytes <br> };
	
	$informacion = shift @free;
	
	my ($tipo, $total, $usado, $libre, $compartido, $buffers, $cached) = split(/\s+/, $informacion);
	
	$capacidad = (($usado/$total) * 100);
	if ($capacidad < 50) {
		$color = $color_texto;
	} elsif ($capacidad < 90) {
		$color = $color_aviso;
	} else {
		$color = $color_peligro;
	}
	
	print "<FONT COLOR=$color_cabecera>Swap memory used: </FONT><FONT COLOR=$color>";

	$capacidad = int($capacidad);
	$capacidad =~ s/$capacidad/$capacidad%/;
	print ("Used memory: ", $capacidad);
	print "</FONT> &nbsp; &nbsp;--->  &nbsp; &nbsp;  Total Memory: $total Kbytes  --  Free Memory: $libre Kbytes \n";
	print "<br>\n";		
		
} # Final de la funcion espacio libre


sub usuarios {

	@who = `w`;
	shift @who;
	
	$titulo_cabecera = shift @who;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=who></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Show who is logged on and what they are doing: </FONT> w";
	print "<br><br>";
	print "<table style='text-align: left; width: 70%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
	
	
	foreach $usuariostotal (@who) {
		print "<tr>";
		@usuarios= split (/\s+/, $usuariostotal);
		foreach $usuario (@usuarios) {
			print "<td><font size=$tam_fuente>$usuario</FONT></td>";
		}
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion usuarios




# Funcion tail
sub tail {
   print "<a name=tail></a>";
	print "<br><br><br>";
	
   @tail = `tail -100 /root/.bash_history`; # As root 

	print "<FONT COLOR=$color_cabecera>Last 100 commands executed: </FONT> tail -100 \$HOME/.bash_history"; 
	print "<br>";	

	foreach $t (@tail) {
		@comandos= split (/\s+/, $t);
		foreach $comando (@comandos) {
			print "<br><font size=$tam_fuente>$comando</FONT>";
		}
	}
	

} # Final de la funcion tail


# Funcion last
sub last {
   print "<a name=last></a>";
	print "<br><br><br>";
	
   @last = `last`;
	pop @last; # Eliminamos la ultima linea del array
	print "<FONT COLOR=$color_cabecera>Show listing of last logged in users: </FONT> last"; 
	print "<br><br>";
		
	print "<table style='text-align: left; width: 70%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $l (@last) {
		print "<tr>";
		@ultimos= split (/\s+/, $l);
		foreach $logins (@ultimos) {
			print "<td><font size=$tam_fuente>$logins</FONT></td>";
		}
		print "</tr>";
	}

	print "</tbody>";
	print "</table>";
	
}# Final de la funcion last



# Funcion grub
sub grub {

   print "<a name=grub></a>";
	print "<br><br><br>";
	my $fichero="/boot/grub/menu.lst";

	if (!open(TEXTFILE, $fichero)) {
   	 print "No se puede abrir el fichero $fichero <br>";
	}	

	print "<FONT COLOR=$color_cabecera>Grub configuration: </FONT> /boot/grub/menu.lst"; 
	print "<br><br>";
	while (<TEXTFILE>) {
          print "$_ <br>";
   }
	
} # Final de la funcion grub


# Funcion rc3
sub rc3 {
   #$rc3 = `ls /etc/rc.d/rc3.d/S*`; # In redhat
   @rc3 = `ls /etc/rc3.d/S*`; # In debian
   print "<a name=rc3></a>";
	print "<br><br><br>";

	print "<FONT COLOR=$color_cabecera>Started applications in the rc3.d level: </FONT> /etc/rc3.d/"; 
	print "<br><br>";
	
	foreach $r (@rc3) {
		@S= split (/\s+/, $r);
		foreach $S3 (@S) {
			print "<br><font size=$tam_fuente>$S3</FONT>";
		}
	}


} # Final de la funcion rc3



sub runShells {
   @SHELLS=`cat /etc/passwd | grep -v \/false | grep -v \/nologin | grep -v \/shutdown | grep -v \/halt | grep -v \/sync | grep -v \/news`;
   print "<a name=shells></a>";
	print "<br><br><br>";

	print "<FONT COLOR=$color_cabecera>Active users in the system with an active shell:</FONT>"; 
	print "<br>";
	
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $s (@SHELLS) {
		print "<tr>";
		@S= split (/\s+/, $s);
		foreach $shellsactivas (@S) {
			print "<td><font size=$tam_fuente>$shellsactivas</FONT></td>"
		}
		print "</tr>";
	}

	print "</tbody>";
	print "</table>";
	
}









# Funcion que visualiza el espacio en disco
sub espacio_disco {

	@df = `df -h`;
	$titulo_cabecera = shift @df;
	
	my ($sistemaficheros, $tam, $usado, $disp, $uso, $montado, $en)= split (/\s+/, $titulo_cabecera);
	
	print "<a name=df></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>File system disk space usage: </FONT>df -h";
	print "<br><br>";
	print "<table style='text-align: left; width: 70%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$sistemaficheros</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$tam</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$usado</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$disp</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$uso</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$montado $en</FONT></td>\n";
	print "</tr>";
  
	foreach $espaciototal (@df) {
		print "<tr>";

		my ($sistemaficheros, $tam, $usado, $disp, $capacidad, $montado)= split (/\s+/, $espaciototal);

		$capacidad =~ s/%//;
		if ($capacidad < 50) {
			$color = $color_texto;
		} elsif ($capacidad < 90) {
			$color = $color_aviso;
		} else {
			$color = $color_peligro;
		}
		
		$capacidad =~ s/$capacidad/$capacidad%/;
		
		print "<td><font size=$tam_fuente>$sistemaficheros</td><td><font size=$tam_fuente>$tam</td><td><font size=$tam_fuente>$usado</td><td><font size=$tam_fuente>$disp</td><td><FONT COLOR=$color><font size=$tam_fuente>$capacidad</FONT></td><td><font size=$tam_fuente>$montado</td>";
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";
	print "<br>\n";
  
} # Final de la funcion espacio_disco


# Funcion que visualiza el espacio en disco
sub espacio_inodos {

	@dfi = `df -i`; # En solaris seria df -o i
	$titulo_cabecera = shift @dfi;
	
	my ($sistemaficheros, $inodos, $usado, $libres, $uso, $montado, $en)= split (/\s+/, $titulo_cabecera);
	
	print "<a name=dfi></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>File system inodes usage: </FONT>df -i";
	print "<br><br>";
	print "<table style='text-align: left; width: 70%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$sistemaficheros</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$inodos</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$usado</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$libres</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$uso</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$montado $en</FONT></td>\n";
	print "</tr>";
  
	foreach $espaciototal (@dfi) {
		print "<tr>";

		my ($sistemaficheros, $inodos, $usado, $libres, $capacidad, $montado)= split (/\s+/, $espaciototal);

		$capacidad =~ s/%//;
		if ($capacidad < 50) {
			$color = $color_texto;
		} elsif ($capacidad < 90) {
			$color = $color_aviso;
		} else {
			$color = $color_peligro;
		}
		
		$capacidad =~ s/$capacidad/$capacidad%/;
		
		print "<td><font size=$tam_fuente>$sistemaficheros</td><td><font size=$tam_fuente>$inodos</td><td><font size=$tam_fuente>$usado</td><td><font size=$tam_fuente>$libres</td><td><FONT COLOR=$color><font size=$tam_fuente>$capacidad</FONT></td><td><font size=$tam_fuente>$montado</td>";
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";
	print "<br>\n";
  
} # Final de la funcion espacio_inodos



sub nmap {
	@nmap = `nmap localhost`;
	shift @nmap;
	shift @nmap;
	shift @nmap;
	shift @nmap;
	$titulo_cabecera = shift @nmap;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=nmap></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Opened ports in the system: </FONT> nmap localhost";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
 
	pop @nmap; # Eliminamos la ultima linea del array

	foreach $puertostotales (@nmap) {
		print "<tr>";
		@puertos= split (/\s+/, $puertostotales);
		foreach $puerto (@puertos) {
			print "<td><font size=$tam_fuente>$puerto</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion nmap



sub procesos {

	open (PS,"/bin/ps aux | /usr/bin/sort -rn +4 |");
	@ps = <PS>;
	close (PS);
 
	$tituloscabecera = pop (@ps); ## get rid of last line
	@titulos= split (/\s+/, $tituloscabecera);
	
	print "<a name=ps></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Processes running in the system:: </FONT> ps aux | /usr/bin/sort -rn +4";
	print "<br><br>";
	print "<table style='text-align: left; width: 100%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";

	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
  
	foreach $procesostotales (@ps) {
		print "<tr>";
		@procesos= split (/\s+/, $procesostotales);
		$i=0;
		foreach $proceso (@procesos) {
			$i=$i+1;
			if ($i>11) { last; } # Numero de parametros visibles de los procesos que se estan ejecutando 
			print "<td><font size=$tam_fuente>$proceso</FONT></td>";
		}
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";
	print "<br>\n";
 
} # Final de la funcion procesos




sub rpms {
   #@rpm = `rpm -qa`; # In redhat
   @rpms = `dpkg --get-selections`; # In debian
   print "<a name=rpms></a>";
	print "<br><br><br>";

	print "<FONT COLOR=$color_cabecera>Installed packages in the system: </FONT> dpkg --get-selections"; 
	print "<br><br>";
	
	foreach $r (@rpms) {
			print "<br><font size=$tam_fuente>$r</FONT>";
	}


} # Final de la funcion rpms





sub top {

   @top = `top -n1 -b1`;

	print "<a name=top></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Dynamic real-time view of a running system: </FONT> top -n1 -b1";
	print "<br><br>";
	
	$top1 = shift (@top); 
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top1</FONT>";
	$top2 = shift(@top);
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top2</FONT>";
	$top3 = shift(@top); 
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top3</FONT>";
	$top4 = shift (@top); 
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top4</FONT>";
	$top5 = shift (@top); 
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top5</FONT><br><br><br>";
	
	print "<table style='text-align: left; width: 80%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	shift (@top);
	
	$tituloscabeceratop = shift (@top); ## get rid of last line
	@titulos= split (/\s+/, $tituloscabeceratop);
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
  
	foreach $procesostotales (@top) {
		print "<tr>";
		@procesos= split (/\s+/, $procesostotales);
		$i=0;
		foreach $proceso (@procesos) {
			$i=$i+1;
			if ($i>13) { last; } # Numero de parametros visibles de los procesos que se estan ejecutando 
			print "<td><font size=$tam_fuente>$proceso</FONT></td>";
		}
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";
	print "<br>\n";
 
} # Final de la funcion top














sub rpcinfo {
	@rpc = `rpcinfo -p localhost`;

	$titulo_cabecera = shift @rpc;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=rpc></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Openned RPC services: </FONT> rpcinfo -p localhost";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
 
	foreach $servicios (@rpc) {
		print "<tr>";
		@servicio= split (/\s+/, $servicios);
		foreach $r (@servicio) {
			print "<td><font size=$tam_fuente>$r</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion rpcinfo



sub setuid {
	@setuid = `find / -type f -perm -4000 -print 2>/dev/null`;

	print "<a name=setuid></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Files with setuid permissions: </FONT>find / -type f -perm -4000 -print 2>/dev/null";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@setuid) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion setuid


sub setgid {
	@setgid = `find / -type f -perm -2000 -print 2>/dev/null`;

	print "<a name=setgid></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Files with setgid permissions: </FONT>find / -type f -perm -2000 -print 2>/dev/null";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@setgid) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion setgid


sub rhosts {
	@rhosts = `find / -type f -name .rhosts -print 2>/dev/null`;

	print "<a name=rhost></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>.rhosts files: </FONT>find / -type f -name .rhosts -print 2>/dev/null";
	print "<br><FONT COLOR=$color_cabecera>The machines and users writen in these files they will have allowed to access without password using some r-service, like rlogin</FONT>";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@rhosts) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion rhosts



sub ficheroescritura {
	@ficheroescritura  = `find / -type f -perm -2 -print 2>/dev/null`;

	print "<a name=ficheroescritura ></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Files with write access for all users: </FONT>find / -type f -perm -2 -print 2>/dev/null";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@ficheroescritura ) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion ficheroescritura 




sub ficheros777 {
	@ficheros777  = `find / -type f -perm 777`;

	print "<a name=ficheros777></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Files with 777 permissions: </FONT>find / -type f -perm 777";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@ficheros777 ) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion ficheros777




sub dir777 {
	@dir777  = `find / -type d -perm 777`;

	print "<a name=dir777></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Folders with 777 permissions: </FONT>find / -type d -perm 777";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";

	foreach $directorios (@dir777 ) {
		print "<tr>";
		@directorio= split (/\s+/, $directorios);
		foreach $d (@directorio) {
			print "<td><font size=$tam_fuente>$d</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion dir777




sub runFilesNoGroup {
	@runFilesNoGroup  = `find /  -nouser -o -nogroup`;

	print "<a name=runFilesNoGroup></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Files without group: </FONT>find /  -nouser -o -nogroup";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";

	foreach $ficheros (@runFilesNoGroup ) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion runFilesNoGroup





sub tmp {
	my $dir="/tmp/";
   my @flist;

	print "<a name=tmp></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>/tmp content: </FONT>ls -ltr /tmp";
	print "<br><br>";


	if (opendir(DIRH,"$dir")){
                @flist=readdir(DIRH);
                closedir DIRH;
                foreach (@flist){
                	# ignorar . y .. :
                	next if ($_ eq "." || $_ eq "..");
                	print "<br><font size=$tam_fuente>$dir/$_</FONT>";
			 		 }
	}else{
		print "ERROR: Is not possible to load $dir\n";
	}




} # Final de la funcion tmp
























sub rutas {
	@rutas = `netstat -nr`;
	shift @rutas;
	
	$titulo_cabecera = shift @rutas;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=rutas></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Routing tables: </FONT> netstat -nr";
	print "<br><br>";
	print "<table style='text-align: left; width: 50%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
 
	foreach $rutasred (@rutas) {
		print "<tr>";
		@ruta= split (/\s+/, $rutasred);
		foreach $r (@ruta) {
			print "<td><font size=$tam_fuente>$r</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion rutas




sub activeconections {
	@activeconections = `netstat -ta`;
	shift @activeconections;
	
	$titulo_cabecera = shift @activeconections;
	$titulo_cabecera =~ s/Local Address/Local_Address/;
	$titulo_cabecera =~ s/Foreign Address/Foreign_Address/;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=activeconections></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Active Internet connections (servers and established): </FONT> netstat -ta";
	print "<br><br>";
	print "<table style='text-align: left; width: 60%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
 
	foreach $rutasred (@activeconections) {
		print "<tr>";
		@ruta= split (/\s+/, $rutasred);
		foreach $r (@ruta) {
			print "<td><font size=$tam_fuente>$r</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion activeconections





sub IP{
	$interface_info =`ifconfig -a`;
	@ifcfg= split (/\s+/, $interface_info); # separamos los interfaces de red
	@eth_addr = split /(inet)\s(addr:)([0-9.]*)(.)*(Mask:)([0-9.]*)/,$interface_info ; # separamos las direcciones ip de los interfaces de red
	#@eth_addr = split /(inet)\s(addr:)([0-9.]*)/,$interface_info ; # separamos las direcciones ip de los interfaces de red
	
	print "<a name=ifconfig></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Status of the currently active interfaces : </FONT>ifconfig -a";
	print "<br><br>";
	
	print "<font size=$tam_fuente COLOR=$color_cabecera>Interfaces:</FONT><br>";
	foreach $i (@ifcfg) {
			if ($i =~ /^eth/){
				$interface_info =`ifconfig $i`;
				@eth_addr = split /(inet)\s(addr:)([0-9.]*)(.)*(Mask:)([0-9.]*)/,$interface_info ;
				$addr = $eth_addr[3];
				print "<font size=$tam_fuente>$i: $addr </FONT><br>";
			}
	}
	
	print "<br><br>";
	print "<font size=$tam_fuente COLOR=$color_cabecera>Virtual network interfaces:</FONT><br>";
	foreach $i (@ifcfg) {
			if ($i =~ /^vmn/){
				$interface_info =`ifconfig $i`;
				@eth_addr = split /(inet)\s(addr:)([0-9.]*)(.)*(Mask:)([0-9.]*)/,$interface_info ;
				$addr = $eth_addr[3];
				print "<font size=$tam_fuente>$i: $addr </FONT><br>";
			}
	}
} # Final de la funcion IP





# --------- Seguridad -----------#





sub compruebaShells {
   my $ficheroshell="/etc/shells";
   $verificashell=0;

	print "<a name=compruebaShells></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Shells system status: </FONT>/etc/shells";
	print "<br><br>";
	

        if (!open(TEXTFILESHELL, $ficheroshell)) {
                print "No se puede abrir el fichero $ficheroshell <br><br>";
        }

         while (<TEXTFILESHELL>) {
                if (!open(TEXTFILESHELL2, $_)) {
                	if ($_ =~ /^#/)
                		{
                			print "<br>";
                		}
                	else {
                			$color = $color_peligro;
                        print "<FONT COLOR=$color> The shell $_ is not in the system but is allowed their usage, it is included in /etc/shells !!! </font><br>";
                        $verificashell=$verificashell+1;
                  }
                 }
         }
        if ($verificashell==0) {
                print "<br> OK, all shells accounts estan being used <br>";
        }



} # Fin compruebaShells






sub checkNoRootSSH {


	print "<a name=compruebaShells></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>ssh root access: </FONT>/etc/ssh/sshd_config";
	print "<br><br>";
	
	my $fichero="/etc/ssh/sshd_config";
	my $cadena="#PermitRootLogin no";
	my $cadena2="PermitRootLogin yes";
	my $cadena3="#PermitEmptyPasswords no";
	my $cadena4="PermitEmptyPasswords yes";
	@cadenasabuscar=("$cadena", "$cadena2" , "$cadena3", "$cadena4");

	$verifica=0;

	if (!open(TEXTFILE, $fichero)) {
   	print "It is not possible to read $fichero <br>";
	}

 	while (<TEXTFILE>) {
   	 foreach $word (@cadenasabuscar) {
      	if (index($_, $word) >= 0) {
        		$verifica=$verifica+1;
      	}
    	}
  	}

	if ($verifica>0) {
  		print  "<FONT COLOR=$color_peligro> DANGER: The next file has some of the next text $fichero: <br> </FONT>";
  		print  "<FONT COLOR=$color_peligro>  $cadena <br> $cadena2 <br> $cadena3 <br> $cadena4 <br><br> </FONT>";
  		print  "<FONT COLOR=$color_peligro> It is allowed the ssh root login or null passwords are allowed, to solve you must <br></FONT>";
  		print  "<FONT COLOR=$color_peligro> it is neccesary to edit and descomment the next line #PermitRootLogin no and #PermitEmptyPasswords no<br><br></FONT>";
	}
	else{
  		print  "OK- The next file does not contain any of the following chains :  $cadena | $cadena2 | $cadena3 | $cadena4: $fichero <br>";
  		print  "OK- ssh is not allowd to root user<br><br>";
	}

}# Fin checkNoRootSSH 





sub checkDisabledCtrlAltDel {

	print "<a name=compruebaShells></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>ctrl+alt+del reboot command check: </FONT>/etc/ssh/sshd_config";
	print "<br><br>";
	

	my $fichero="/etc/inittab";
	my $cadena="#ca::ctrlaltdel";
	@cadenasabuscar=("$cadena");

	$verifica=0;

	if (!open(TEXTFILE, $fichero)) {
    	print "It is not possible to open $fichero <br>";
	}

 	while (<TEXTFILE>) {
   	 foreach $word (@cadenasabuscar) {
      	if (index($_, $word) >= 0) {
        	$verifica=$verifica+1;
      	}
    	}
  	}

	if ($verifica>0) {
  		print "OK- The next file contains  $cadena: $fichero <br>";
  		print "OK- Is is not allowed the system reboot with ctrl+alt+del <br><br>";
	}
	else{
  		print "<FONT COLOR=$color_peligro>DANGER: The next file does not containt some of the next text $cadena: $fichero <br><br></FONT>";
  		print "<FONT COLOR=$color_peligro>It is allowed the machine restart with ctrl+alt+del <br></FONT>";
  		print "<FONT COLOR=$color_peligro>To solve you must edit and descomment the next line ca::ctrlaltdel:/sbin/shutdown -t3 -r now <br></FONT>";
	}


}# Fin checkDisabledCtrlAltDel




sub checkCrontab {

	print "<a name=checkCrontab></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Users crontab use: </FONT>/etc/cron.allow";
	print "<br><br>";
	
	my $fichero="/etc/cron.allow";
	my $cadena="root";
	@cadenasabuscar=("$cadena");

	$verifica=0;

	if (!open(TEXTFILE, $fichero)) {
   	 print "It is not possible to open $fichero, It is allowed the crontab use for all users  !!!! <br><br>";
	}

 	while (<TEXTFILE>) {
   	 foreach $word (@cadenasabuscar) {
      	if (index($_, $word) >= 0) {
        		$verifica=$verifica+1;
	      }
   	 }
  	}

	if ($verifica>0) {
  		print "OK- The next file contains $cadena: $fichero <br>";
  		print "OK- The crontab it is only allowed their use for root user <br><br>";
	}
	else{
  		print "<FONT COLOR=$color_peligro>DANGER: The next file does not contains some of the next text $cadena: $fichero <br><br></FONT>";
  		print "<FONT COLOR=$color_peligro>It is allowed the crontab use for all users  !!!!<br></FONT>";
  		print "<FONT COLOR=$color_peligro>To solve you must edit and add root to the next file $fichero<br></FONT>";
	}


}# Fin checkCrontab




sub checkApache {


	print "<a name=checkApache></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Apache configuration: </FONT>";
	print "<br><br>";
	
	my $apache1="/etc/httpd/conf/httpd.conf";
	my $apache2="/etc/apache2/apache2.conf";
	my $apache3="/etc/apache/apache.conf";

	
   if (open(TEXTFILE, $apache1)) {
   	print "<FONT COLOR=$color_cabecera>Apache RedHat: </FONT> /etc/httpd/conf/httpd.conf <br><br>";
   	while (<TEXTFILE>) {
   		print "$_<br>";
  		}
   }
   elsif (open(TEXTFILE, $apache2)) {
   		print "<FONT COLOR=$color_cabecera>Apache 2 Debian: </FONT> /etc/apache2/apache2.conf <br><br>";
   		while (<TEXTFILE>) {
   			print "$_<br>";
  			}
   	}
   
	elsif (open(TEXTFILE, $apache3)) {
			print "<FONT COLOR=$color_cabecera>Apache 1 Debian: </FONT> /etc/apache/apache.conf <br><br>";
  			while (<TEXTFILE>) {
  				print "$_<br>";
  			}
   	}
   
   else {
   	print "<FONT COLOR=$color_peligro>Apache is not installed </FONT><br><br>";
   }
   


}# Fin checkApache






sub recomendaciones {


	print "<a name=recomendaciones></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Security software recommendations: </FONT>";
	print "<br><br>";
	

	
   print "We can use the next security applications to check the system status, file system and integrity<br><br>";


   print "<FONT COLOR=$color_cabecera>- chkrootkit: shell script that checks system binaries for rootkit modification</FONT> http://www.chkrootkit.org/<br>";
   print "<FONT COLOR=$color_cabecera>- AIDE (Advanced Intrusion Detection Environment) </FONT>http://www.cs.tut.fi/~rammer/aide.html<br>";
   print "<FONT COLOR=$color_cabecera>- John the Ripper is a fast password cracker </FONT>http://www.openwall.com/john/ <br>";
   print "<FONT COLOR=$color_cabecera>- Logcheck is a simple utility which is designed to allow a system administrator to view the logfiles which are produced upon hosts under their control. </FONT>http://logcheck.org/ <br>";
   print "<FONT COLOR=$color_cabecera>- Portsentry is an attack detection tool </FONT>http://sourceforge.net/projects/sentrytools/<br>";
   print "<FONT COLOR=$color_cabecera>- HostSentry is a host based intrusion detection tool </FONT><br>";
   print "<FONT COLOR=$color_cabecera>- DenyHosts is a script intended to be run by Linux system administrators to help thwart SSH server attacks  </FONT>http://denyhosts.sourceforge.net/<br>";


}# Fin recomendaciones seguridad

