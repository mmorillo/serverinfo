#!/usr/bin/perl

# Server-Info.cgi : CGI que permite visualizar mediante el navegador informacion de tu sistema
# =~  is a special operator that invokes the regular expression engine. 
# Podemos ver las variables de entorno usadas por perl:
#   foreach $key (sort keys(%ENV)) {
#      print "$key = $ENV{$key}<p>";
#   }


# GMT a local
use Time::Local;
#use strict;
#use CGI ':standard';
#use Net::Ifconfig::Wrapper; # deber estar instalada la libreria libnet-ip-perl y libnet-ifconfig-wrapper-perl

# --------------- Definicion de variables -----------------#

$ENV{PATH} = "/sbin:/usr/local/bin:/usr/bin:/bin"; # Definimos las rutas de los ejecutables, por defecto: /usr/local/bin:/usr/bin:/bin
my $refresco_html = '';  ## indica el numero de segundos para la variable html meta refresh, si dejamos vacio este valor no se refrescara la pagina
my $color_fondo='white';
my $color_cabecera = 'green';
my $color_texto = 'black';
my $color_link = 'blue';
my $vcolor_link = 'purple';
my $color_aviso = 'orange';
my $color_peligro = 'red';
# my $fuente = 'Clean, Fixed, Courier New, Courier, Terminal, Screen';
my $fuente = 'Arial, Helvetica, Geneva';
my $tam_fuente='-1';

my @meses = ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Deciembre");
my ($c_segundo,$c_minuto,$c_hora,$c_dia,$c_mes,$c_ano,$c_wdia,$c_ydia,$c_isdst) = localtime(time);
$c_ano = 1900 + $c_ano;
if ($c_minuto < 10) {
  $c_minuto = "0$c_minuto";
}

# Si hemos definido la variable insertamos el tag correspondiente para realizar el refresco de la pagina html
if ($refresco_html) {
  $tag_refresco = "<META HTTP-EQUIV=Refresh CONTENT=$refresco_html>";
}



# --------------- Inicio de la pagina -----------------#

print "Content-type: text/html; charset=UTF-8\n\n";
print qq{
<HTML>
<HEAD>
<TITLE> Informacion del sistema - $ENV{SERVER_NAME} </TITLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
$tag_refresco
</HEAD>
<BODY BGCOLOR=$color_fondo TEXT=$color_texto LINK=$color_link VLINK=$vcolor_link>
<FONT FACE="$fuente" SIZE="$tam_fuente">

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#F8F8F8">
<tr>
   <td>
   <center>
   <font color="#5A6770" face="$fuente" size=5>
	<b>Informe del servidor: $ENV{SERVER_NAME} - Con fecha: $c_dia-$meses[$c_mes]-$c_ano $c_hora:$c_minuto</b>
   </font>
   </center>
   </td>
</tr>
<tr>
   <td bgcolor="#EEEEEE">
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     Visualización de información del sistema para su control y administración
     </font>
     </center>
     </td>
</tr>
</table>
</center>
   
   
<br><br>

<FONT size=3 COLOR=#5A6770>Información general del sistema:</FONT>
<ol>
<li><a href=#uptime style="color:red; text-decoration:none";>uptime - Tell how long the system has been running</a></li>
<li><a href=#free style="color:red; text-decoration:none";>free - Display amount of free and used memory in the system</a></li>
<li><a href=#who style="color:red; text-decoration:none";>who - show who is logged on</a></li>
<li><a href=#tail style="color:red; text-decoration:none";>tail -100 \$HOME/.bash_history - Last 100 commands executed</a></li>
<li><a href=#last style="color:red; text-decoration:none";>last - show listing of last logged in users</a></li>
<li><a href=#grub style="color:red; text-decoration:none";>grub - GRand Unified Bootloader</a></li>
<li><a href=#rc3 style="color:red; text-decoration:none";>rc3 startup services</a></li>
<li><a href=#shells style="color:red; text-decoration:none";>Active shells in the system</a></li>
</ol>

<br>
<FONT size=3 COLOR=#5A6770>Información del sistema de archivos:</FONT>
<ol> 
<li><a href=#df style="color:red; text-decoration:none";>df -h - report file system disk space usage</a></li>
<li><a href=#dfi style="color:red; text-decoration:none";>df -i - report file system inodes usage</a></li>
<li><a href=#setuid style="color:red; text-decoration:none";>find / -type f -perm -4000 -print 2>/dev/null - ficheros con setuid activo</a></li>
<li><a href=#setuid style="color:red; text-decoration:none";>find / -type f -perm -2000 -print 2>/dev/null - ficheros con setgid activo</a></li>
<li><a href=#rhost style="color:red; text-decoration:none";>find / -type f -name .rhosts -print 2>/dev/null - ficheros .rhosts</a></li>
<li><a href=#ficheroescritura style="color:red; text-decoration:none";>find / -type f -perm -2 -print 2>/dev/null - ficheros con escritura para todos los usuarios</a></li>
<li><a href=#ficheros777 style="color:red; text-decoration:none";>find / -type f -perm 777 - ficheros con permisos 777</a></li>
<li><a href=#dir777 style="color:red; text-decoration:none";>find / -type d -perm 777 - directorios con permisos 777</a></li>
<li><a href=#runFilesNoGroup style="color:red; text-decoration:none";>find /  -nouser -o -nogroup - Ficheros sin grupo</a></li>
<li><a href=#tmp style="color:red; text-decoration:none";>Contenido del directorio /tmp</a></li>
</ol>

<br>
<FONT size=3 COLOR=#5A6770>Información TCP/IP:</FONT>
<ol> 
<li><a href=#nmap style="color:red; text-decoration:none";>nmap - Network exploration tool and security / port scanner</a></li>
<li><a href=#rpc style="color:red; text-decoration:none";>rpcinfo -p localhost - show the RPC services registered on the local machine use</a></li>
<li><a href=#rutas style="color:red; text-decoration:none";>netstat -nr - displays the contents of the routing  tables</a></li>
<li><a href=#activeconections style="color:red; text-decoration:none";>netstat -ta - Active Internet connections (servers and established)</a></li>
<li><a href=#ifconfig style="color:red; text-decoration:none";>ifconfig -a - Configure a network interface and displays the status of the currently active interfaces</a></li>
</ol>

<br>
<FONT size=3 COLOR=#5A6770>Información de los procesos, variables y aplicaciones del sistema:</FONT>
<ol>  
<li><a href=#ps style="color:red; text-decoration:none";>ps - report a snapshot of the current processes</a></li>
<li><a href=#rpms style="color:red; text-decoration:none";>dpkg --get-selections - paquetes instalados en el sistema</a></li>
<li><a href=#top style="color:red; text-decoration:none";>top - display Linux tasks</a></li>
</ol> 

<br>
<FONT size=3 COLOR=#5A6770>Informe de seguridad:</FONT>
<ol>  
<li><a href=#compruebaShells style="color:red; text-decoration:none";>Comprueba el estado de /etc/shells</a></li>
<li><a href=#checkNoRootSSH style="color:red; text-decoration:none";>Chequeo seguridad servidor sshd</a></li>
<li><a href=#checkDisabledCtrlAltDel style="color:red; text-decoration:none";>Comprobacion deshabilitacion CtrlAltDel</a></li>
<li><a href=#checkCrontab style="color:red; text-decoration:none";>Chequeo seguridad de crontab</a></li>
<li><a href=#checkApache style="color:red; text-decoration:none";>Chequeo configuracion apache</a></li>
<li><a href=#recomendaciones style="color:red; text-decoration:none";>Recomendaciones aplicaciones seguridad</a></li>
</ol> 

<br><br>
};

print qq{
<hr color=#AB1517>  <!-- línea de separación roja -->
};



# --------- En este punto llamamos a las funciones para generar la informacion -----------#


print qq{
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     Información general del sistema
     </font>
     </center>
     </td>
</tr>
</table>
</center>
};


&uptime;
&espacio_libre;
&usuarios;
&tail;
&last;
&grub;
&rc3;
&runShells;




print qq{
<br><br><br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     Información del sistema de archivos
     </font>
     </center>
     </td>
</tr>
</table>
</center>

};

&espacio_disco;
&espacio_inodos;
&setuid;
&setgid;
&rhosts;
&ficheroescritura;
&ficheros777;
&dir777;
&runFilesNoGroup;
&tmp;


print qq{
<br><br><br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     Información TCP/IP
     </font>
     </center>
     </td>
</tr>
</table>
</center>
};

&nmap;
&rpcinfo;
&rutas;
&activeconections;
&IP;





print qq{
<br><br><br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     Información de los procesos, variables y aplicaciones del sistema
     </font>
     </center>
     </td>
</tr>
</table>
</center>

};

&procesos;
&rpms;
&top;



print qq{
<br><br><br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
<br><br>

<center>
<table width="80%" cellspacing="1" cellpadding="3" border="0" bgcolor="#EEEEEE">

<tr>
     <td>
     <center>
     <font face="arial, verdana, helvetica" size=4 COLOR=#5A6770>
     Informe de seguridad 
     </font>
     </center>
     </td>
</tr>
</table>
</center>

};

&compruebaShells;
&checkNoRootSSH;
&checkDisabledCtrlAltDel;
&checkCrontab;
&checkApache;
&recomendaciones;



# --------- Fin llamadas a funciones -----------#


print qq{
<br><br>
<hr color=#AB1517>  <!-- línea de separación roja -->
</FONT>
</BODY>
</HTML>
};



# --------- final de la parte principal -----------#






# --------- Definicion de funciones -----------#

# Funcion que define el tiempo que lleva el sistema levantado
sub uptime {

	chop($uptime = `uptime`); # Eliminamos el retorno de carro
	$uptime =~ s/\t/     /g;
	$uptime =~ s/\s/\&nbsp\;/g;

	print qq{
		<a name=uptime></a>
		<br><br><br>
		<FONT COLOR=$color_cabecera>Información del estado y tiempo que lleva el sistema funcionando: </FONT>uptime
		<br><br>
		<FONT COLOR=$color_cabecera>Estado del sistema: </FONT> $uptime
		<br>
	};

} # Final de la funcion uptime


# Funcion que visualiza la memoria libre y usuada del sistema
sub espacio_libre {

	@free = `free -o`;
	$informacion = shift @free; 
	$informacion = shift @free;
	
	my ($tipo, $total, $usado, $libre, $compartido, $buffers, $cached) = split(/\s+/, $informacion); # separamos en variables los datos obtenidos (separados por espacios)

	$capacidad = (($usado/$total) * 100);
	if ($capacidad < 50) {
		$color = $color_texto;
	} elsif ($capacity < 90) {
		$color = $color_aviso;
	} else {
		$color = $color_peligro;
	}
	print "<a name=free></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información de la memoria usada y libre del sistema:</FONT> free -o";
	print "<br><br>\n";	
	print "<FONT COLOR=$color_cabecera>Uso de memoria: </FONT><FONT COLOR=$color>";
	
	$capacidad = int($capacidad);
	$capacidad =~ s/$capacidad/$capacidad%/;
	print ("Memoria usada: ", $capacidad);
	
	print qq{ </FONT> &nbsp; &nbsp;--->  &nbsp; &nbsp;  Memoria total: $total Kbytes  --  Memoria libre: $libre Kbytes <br> };
	
	$informacion = shift @free;
	
	my ($tipo, $total, $usado, $libre, $compartido, $buffers, $cached) = split(/\s+/, $informacion);
	
	$capacidad = (($usado/$total) * 100);
	if ($capacidad < 50) {
		$color = $color_texto;
	} elsif ($capacidad < 90) {
		$color = $color_aviso;
	} else {
		$color = $color_peligro;
	}
	
	print "<FONT COLOR=$color_cabecera>Uso de memoria Swap: </FONT><FONT COLOR=$color>";

	$capacidad = int($capacidad);
	$capacidad =~ s/$capacidad/$capacidad%/;
	print ("Memoria usada: ", $capacidad);
	print "</FONT> &nbsp; &nbsp;--->  &nbsp; &nbsp;  Memoria total: $total Kbytes  --  Memoria libre: $libre Kbytes \n";
	print "<br>\n";		
		
} # Final de la funcion espacio libre


sub usuarios {

	@who = `w`;
	shift @who;
	
	$titulo_cabecera = shift @who;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=who></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información de los usuarios que estan logueados en el sistema: </FONT> w";
	print "<br><br>";
	print "<table style='text-align: left; width: 70%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
	
	
	foreach $usuariostotal (@who) {
		print "<tr>";
		@usuarios= split (/\s+/, $usuariostotal);
		foreach $usuario (@usuarios) {
			print "<td><font size=$tam_fuente>$usuario</FONT></td>";
		}
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion usuarios




# Funcion tail
sub tail {
   print "<a name=tail></a>";
	print "<br><br><br>";
	
   @tail = `tail -100 /root/.bash_history`; # As root 

	print "<FONT COLOR=$color_cabecera>Ultimos 100 comandos ejecutados (por el usuario root) : </FONT> tail -100 \$HOME/.bash_history"; 
	print "<br>";	

	foreach $t (@tail) {
		@comandos= split (/\s+/, $t);
		foreach $comando (@comandos) {
			print "<br><font size=$tam_fuente>$comando</FONT>";
		}
	}
	

} # Final de la funcion tail


# Funcion last
sub last {
   print "<a name=last></a>";
	print "<br><br><br>";
	
   @last = `last`;
	pop @last; # Eliminamos la ultima linea del array
	print "<FONT COLOR=$color_cabecera>Ultimas entradas de usuario en el sistema: </FONT> last"; 
	print "<br><br>";
		
	print "<table style='text-align: left; width: 70%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $l (@last) {
		print "<tr>";
		@ultimos= split (/\s+/, $l);
		foreach $logins (@ultimos) {
			print "<td><font size=$tam_fuente>$logins</FONT></td>";
		}
		print "</tr>";
	}

	print "</tbody>";
	print "</table>";
	
}# Final de la funcion last



# Funcion grub
sub grub {

   print "<a name=grub></a>";
	print "<br><br><br>";
	my $fichero="/boot/grub/menu.lst";

	if (!open(TEXTFILE, $fichero)) {
   	 print "No se puede abrir el fichero $fichero <br>";
	}	

	print "<FONT COLOR=$color_cabecera>Configuracion de grub : </FONT> /boot/grub/menu.lst"; 
	print "<br><br>";
	while (<TEXTFILE>) {
          print "$_ <br>";
   }
	
} # Final de la funcion grub


# Funcion rc3
sub rc3 {
   #$rc3 = `ls /etc/rc.d/rc3.d/S*`; # In redhat
   @rc3 = `ls /etc/rc3.d/S*`; # In debian
   print "<a name=rc3></a>";
	print "<br><br><br>";

	print "<FONT COLOR=$color_cabecera>Procesos que se inician en rc3.d :</FONT> /etc/rc3.d/"; 
	print "<br><br>";
	
	foreach $r (@rc3) {
		@S= split (/\s+/, $r);
		foreach $S3 (@S) {
			print "<br><font size=$tam_fuente>$S3</FONT>";
		}
	}


} # Final de la funcion rc3



sub runShells {
   @SHELLS=`cat /etc/passwd | grep -v \/false | grep -v \/nologin | grep -v \/shutdown | grep -v \/halt | grep -v \/sync | grep -v \/news`;
   print "<a name=shells></a>";
	print "<br><br><br>";

	print "<FONT COLOR=$color_cabecera>Cuentas existentes en el sistema con shell activo:</FONT>"; 
	print "<br>";
	
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $s (@SHELLS) {
		print "<tr>";
		@S= split (/\s+/, $s);
		foreach $shellsactivas (@S) {
			print "<td><font size=$tam_fuente>$shellsactivas</FONT></td>"
		}
		print "</tr>";
	}

	print "</tbody>";
	print "</table>";
	
}









# Funcion que visualiza el espacio en disco
sub espacio_disco {

	@df = `df -h`;
	$titulo_cabecera = shift @df;
	
	my ($sistemaficheros, $tam, $usado, $disp, $uso, $montado, $en)= split (/\s+/, $titulo_cabecera);
	
	print "<a name=df></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información del espacio en disco y particiones del sistema: </FONT>df -h";
	print "<br><br>";
	print "<table style='text-align: left; width: 70%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$sistemaficheros</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$tam</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$usado</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$disp</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$uso</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$montado $en</FONT></td>\n";
	print "</tr>";
  
	foreach $espaciototal (@df) {
		print "<tr>";

		my ($sistemaficheros, $tam, $usado, $disp, $capacidad, $montado)= split (/\s+/, $espaciototal);

		$capacidad =~ s/%//;
		if ($capacidad < 50) {
			$color = $color_texto;
		} elsif ($capacidad < 90) {
			$color = $color_aviso;
		} else {
			$color = $color_peligro;
		}
		
		$capacidad =~ s/$capacidad/$capacidad%/;
		
		print "<td><font size=$tam_fuente>$sistemaficheros</td><td><font size=$tam_fuente>$tam</td><td><font size=$tam_fuente>$usado</td><td><font size=$tam_fuente>$disp</td><td><FONT COLOR=$color><font size=$tam_fuente>$capacidad</FONT></td><td><font size=$tam_fuente>$montado</td>";
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";
	print "<br>\n";
  
} # Final de la funcion espacio_disco


# Funcion que visualiza el espacio en disco
sub espacio_inodos {

	@dfi = `df -i`; # En solaris seria df -o i
	$titulo_cabecera = shift @dfi;
	
	my ($sistemaficheros, $inodos, $usado, $libres, $uso, $montado, $en)= split (/\s+/, $titulo_cabecera);
	
	print "<a name=dfi></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información de los inodos usados por el sistema de ficheros: </FONT>df -i";
	print "<br><br>";
	print "<table style='text-align: left; width: 70%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$sistemaficheros</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$inodos</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$usado</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$libres</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$uso</FONT></td>\n";
	print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$montado $en</FONT></td>\n";
	print "</tr>";
  
	foreach $espaciototal (@dfi) {
		print "<tr>";

		my ($sistemaficheros, $inodos, $usado, $libres, $capacidad, $montado)= split (/\s+/, $espaciototal);

		$capacidad =~ s/%//;
		if ($capacidad < 50) {
			$color = $color_texto;
		} elsif ($capacidad < 90) {
			$color = $color_aviso;
		} else {
			$color = $color_peligro;
		}
		
		$capacidad =~ s/$capacidad/$capacidad%/;
		
		print "<td><font size=$tam_fuente>$sistemaficheros</td><td><font size=$tam_fuente>$inodos</td><td><font size=$tam_fuente>$usado</td><td><font size=$tam_fuente>$libres</td><td><FONT COLOR=$color><font size=$tam_fuente>$capacidad</FONT></td><td><font size=$tam_fuente>$montado</td>";
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";
	print "<br>\n";
  
} # Final de la funcion espacio_inodos



sub nmap {
	@nmap = `nmap localhost`;
	shift @nmap;
	shift @nmap;
	shift @nmap;
	shift @nmap;
	$titulo_cabecera = shift @nmap;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=nmap></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información de los puertos y servicios abiertos en el sistema: </FONT> nmap localhost";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
 
	pop @nmap; # Eliminamos la ultima linea del array

	foreach $puertostotales (@nmap) {
		print "<tr>";
		@puertos= split (/\s+/, $puertostotales);
		foreach $puerto (@puertos) {
			print "<td><font size=$tam_fuente>$puerto</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion nmap



sub procesos {

	open (PS,"/bin/ps aux | /usr/bin/sort -rn +4 |");
	@ps = <PS>;
	close (PS);
 
	$tituloscabecera = pop (@ps); ## get rid of last line
	@titulos= split (/\s+/, $tituloscabecera);
	
	print "<a name=ps></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información de los procesos que estan ejecutandose en el sistema: </FONT> ps aux | /usr/bin/sort -rn +4";
	print "<br><br>";
	print "<table style='text-align: left; width: 100%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";

	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
  
	foreach $procesostotales (@ps) {
		print "<tr>";
		@procesos= split (/\s+/, $procesostotales);
		$i=0;
		foreach $proceso (@procesos) {
			$i=$i+1;
			if ($i>11) { last; } # Numero de parametros visibles de los procesos que se estan ejecutando 
			print "<td><font size=$tam_fuente>$proceso</FONT></td>";
		}
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";
	print "<br>\n";
 
} # Final de la funcion procesos




sub rpms {
   #@rpm = `rpm -qa`; # In redhat
   @rpms = `dpkg --get-selections`; # In debian
   print "<a name=rpms></a>";
	print "<br><br><br>";

	print "<FONT COLOR=$color_cabecera>Paquetes instalados en el sistema:</FONT> dpkg --get-selections"; 
	print "<br><br>";
	
	foreach $r (@rpms) {
			print "<br><font size=$tam_fuente>$r</FONT>";
	}


} # Final de la funcion rpms





sub top {

   @top = `top -n1 -b1`;

	print "<a name=top></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información de los procesos que estan ejecutandose en el sistema: </FONT> top -n1 -b1";
	print "<br><br>";
	
	$top1 = shift (@top); 
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top1</FONT>";
	$top2 = shift(@top);
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top2</FONT>";
	$top3 = shift(@top); 
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top3</FONT>";
	$top4 = shift (@top); 
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top4</FONT>";
	$top5 = shift (@top); 
	print "<br><FONT COLOR=$color_cabecera font size=$tam_fuente>$top5</FONT><br><br><br>";
	
	print "<table style='text-align: left; width: 80%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	shift (@top);
	
	$tituloscabeceratop = shift (@top); ## get rid of last line
	@titulos= split (/\s+/, $tituloscabeceratop);
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
  
	foreach $procesostotales (@top) {
		print "<tr>";
		@procesos= split (/\s+/, $procesostotales);
		$i=0;
		foreach $proceso (@procesos) {
			$i=$i+1;
			if ($i>13) { last; } # Numero de parametros visibles de los procesos que se estan ejecutando 
			print "<td><font size=$tam_fuente>$proceso</FONT></td>";
		}
		print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";
	print "<br>\n";
 
} # Final de la funcion top














sub rpcinfo {
	@rpc = `rpcinfo -p localhost`;

	$titulo_cabecera = shift @rpc;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=rpc></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información de los servicios RPC abiertos en el sistema: </FONT> rpcinfo -p localhost";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
 
	foreach $servicios (@rpc) {
		print "<tr>";
		@servicio= split (/\s+/, $servicios);
		foreach $r (@servicio) {
			print "<td><font size=$tam_fuente>$r</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion rpcinfo



sub setuid {
	@setuid = `find / -type f -perm -4000 -print 2>/dev/null`;

	print "<a name=setuid></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Ficheros con setuid activo: </FONT>find / -type f -perm -4000 -print 2>/dev/null";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@setuid) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion setuid


sub setgid {
	@setgid = `find / -type f -perm -2000 -print 2>/dev/null`;

	print "<a name=setgid></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Ficheros con setgid activo: </FONT>find / -type f -perm -2000 -print 2>/dev/null";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@setgid) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion setgid


sub rhosts {
	@rhosts = `find / -type f -name .rhosts -print 2>/dev/null`;

	print "<a name=rhost></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Ficheros .rhosts: </FONT>find / -type f -name .rhosts -print 2>/dev/null";
	print "<br><FONT COLOR=$color_cabecera>Los nombres de maquina y usuario que se encuentren en estos ficheros tendran permitido el acceso sin contraseña utilizando algun r-servicio, como rlogin</FONT>";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@rhosts) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion rhosts



sub ficheroescritura {
	@ficheroescritura  = `find / -type f -perm -2 -print 2>/dev/null`;

	print "<a name=ficheroescritura ></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Ficheros en los que pueden escribir todos los usuarios: </FONT>find / -type f -perm -2 -print 2>/dev/null";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@ficheroescritura ) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion ficheroescritura 




sub ficheros777 {
	@ficheros777  = `find / -type f -perm 777`;

	print "<a name=ficheros777></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Ficheros con permisos 777: </FONT>find / -type f -perm 777";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
 
	foreach $ficheros (@ficheros777 ) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion ficheros777




sub dir777 {
	@dir777  = `find / -type d -perm 777`;

	print "<a name=dir777></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Directorios con permisos 777: </FONT>find / -type d -perm 777";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";

	foreach $directorios (@dir777 ) {
		print "<tr>";
		@directorio= split (/\s+/, $directorios);
		foreach $d (@directorio) {
			print "<td><font size=$tam_fuente>$d</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion dir777




sub runFilesNoGroup {
	@runFilesNoGroup  = `find /  -nouser -o -nogroup`;

	print "<a name=runFilesNoGroup></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Ficheros sin grupo: </FONT>find /  -nouser -o -nogroup";
	print "<br><br>";
	print "<table style='text-align: left; width: 30%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";

	foreach $ficheros (@runFilesNoGroup ) {
		print "<tr>";
		@fichero= split (/\s+/, $ficheros);
		foreach $f (@fichero) {
			print "<td><font size=$tam_fuente>$f</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion runFilesNoGroup





sub tmp {
	my $dir="/tmp/";
   my @flist;

	print "<a name=tmp></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Contenido del directorio /tmp: </FONT>ls -ltr /tmp";
	print "<br><br>";


	if (opendir(DIRH,"$dir")){
                @flist=readdir(DIRH);
                closedir DIRH;
                foreach (@flist){
                	# ignorar . y .. :
                	next if ($_ eq "." || $_ eq "..");
                	print "<br><font size=$tam_fuente>$dir/$_</FONT>";
			 		 }
	}else{
		print "ERROR: no se puede leer el directorio $dir\n";
	}




} # Final de la funcion tmp
























sub rutas {
	@rutas = `netstat -nr`;
	shift @rutas;
	
	$titulo_cabecera = shift @rutas;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=rutas></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Información de las rutas de red: </FONT> netstat -nr";
	print "<br><br>";
	print "<table style='text-align: left; width: 50%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
 
	foreach $rutasred (@rutas) {
		print "<tr>";
		@ruta= split (/\s+/, $rutasred);
		foreach $r (@ruta) {
			print "<td><font size=$tam_fuente>$r</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion rutas




sub activeconections {
	@activeconections = `netstat -ta`;
	shift @activeconections;
	
	$titulo_cabecera = shift @activeconections;
	$titulo_cabecera =~ s/Local Address/Local_Address/;
	$titulo_cabecera =~ s/Foreign Address/Foreign_Address/;
	@titulos= split (/\s+/, $titulo_cabecera);
	
	print "<a name=activeconections></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Conexiones a Internet activas (servidor y establecidas): </FONT> netstat -ta";
	print "<br><br>";
	print "<table style='text-align: left; width: 60%;' border='0' cellpadding='0' cellspacing='0'>\n";
	print "<tbody>\n";
	print "<tr>\n";
	
	foreach $titulo (@titulos) {
		print "<td><FONT COLOR=$color_cabecera font size=$tam_fuente>$titulo</td></FONT>\n";
	}
	
	print "</tr>";
 
	foreach $rutasred (@activeconections) {
		print "<tr>";
		@ruta= split (/\s+/, $rutasred);
		foreach $r (@ruta) {
			print "<td><font size=$tam_fuente>$r</FONT></td>";
		}
	print "</tr>";
	}
	
	print "</tbody>";
	print "</table>";

} # Final de la funcion activeconections





sub IP{
	$interface_info =`ifconfig -a`;
	@ifcfg= split (/\s+/, $interface_info); # separamos los interfaces de red
	@eth_addr = split /(inet)\s(addr:)([0-9.]*)(.)*(Mask:)([0-9.]*)/,$interface_info ; # separamos las direcciones ip de los interfaces de red
	#@eth_addr = split /(inet)\s(addr:)([0-9.]*)/,$interface_info ; # separamos las direcciones ip de los interfaces de red
	
	print "<a name=ifconfig></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Interfaces y direcciones de red : </FONT>ifconfig -a";
	print "<br><br>";
	
	print "<font size=$tam_fuente COLOR=$color_cabecera>Interfaces de red principales:</FONT><br>";
	foreach $i (@ifcfg) {
			if ($i =~ /^eth/){
				$interface_info =`ifconfig $i`;
				@eth_addr = split /(inet)\s(addr:)([0-9.]*)(.)*(Mask:)([0-9.]*)/,$interface_info ;
				$addr = $eth_addr[3];
				print "<font size=$tam_fuente>$i: $addr </FONT><br>";
			}
	}
	
	print "<br><br>";
	print "<font size=$tam_fuente COLOR=$color_cabecera>Interfaces de red virtuales:</FONT><br>";
	foreach $i (@ifcfg) {
			if ($i =~ /^vmn/){
				$interface_info =`ifconfig $i`;
				@eth_addr = split /(inet)\s(addr:)([0-9.]*)(.)*(Mask:)([0-9.]*)/,$interface_info ;
				$addr = $eth_addr[3];
				print "<font size=$tam_fuente>$i: $addr </FONT><br>";
			}
	}
} # Final de la funcion IP





# --------- Seguridad -----------#





sub compruebaShells {
   my $ficheroshell="/etc/shells";
   $verificashell=0;

	print "<a name=compruebaShells></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Estado de las cuentas de shell del sistema: </FONT>/etc/shells";
	print "<br><br>";
	

        if (!open(TEXTFILESHELL, $ficheroshell)) {
                print "No se puede abrir el fichero $ficheroshell <br><br>";
        }

         while (<TEXTFILESHELL>) {
                if (!open(TEXTFILESHELL2, $_)) {
                	if ($_ =~ /^#/)
                		{
                			print "<br>";
                		}
                	else {
                			$color = $color_peligro;
                        print "<FONT COLOR=$color> La shell $_ no existe en el sistema pero esta permitido su uso, esta incluida en /etc/shells, fallo Seguridad !!! </font><br>";
                        $verificashell=$verificashell+1;
                  }
                 }
         }
        if ($verificashell==0) {
                print "<br> OK, Todas las cuentas shell del sistema se estan utilizando <br>";
        }



} # Fin compruebaShells






sub checkNoRootSSH {


	print "<a name=compruebaShells></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Chequeo del acceso por ssh al usuario root: </FONT>/etc/ssh/sshd_config";
	print "<br><br>";
	
	my $fichero="/etc/ssh/sshd_config";
	my $cadena="#PermitRootLogin no";
	my $cadena2="PermitRootLogin yes";
	my $cadena3="#PermitEmptyPasswords no";
	my $cadena4="PermitEmptyPasswords yes";
	@cadenasabuscar=("$cadena", "$cadena2" , "$cadena3", "$cadena4");

	$verifica=0;

	if (!open(TEXTFILE, $fichero)) {
   	print "No se puede abrir el fichero $fichero <br>";
	}

 	while (<TEXTFILE>) {
   	 foreach $word (@cadenasabuscar) {
      	if (index($_, $word) >= 0) {
        		$verifica=$verifica+1;
      	}
    	}
  	}

	if ($verifica>0) {
  		print  "<FONT COLOR=$color_peligro> PELIGRO: El siguiente fichero contiene alguna de las siguientes cadenas en el fichero $fichero: <br> </FONT>";
  		print  "<FONT COLOR=$color_peligro>  $cadena <br> $cadena2 <br> $cadena3 <br> $cadena4 <br><br> </FONT>";
  		print  "<FONT COLOR=$color_peligro> Esta permitido el acceso ssh al usario root o el acceso con claves vacias, para denegar el acceso es <br></FONT>";
  		print  "<FONT COLOR=$color_peligro> necesario descomentar la linea #PermitRootLogin no y descomentar #PermitEmptyPasswords no<br><br></FONT>";
	}
	else{
  		print  "OK- El siguiente fichero NO contiene ninguna de las siguientes cadenas:  $cadena | $cadena2 | $cadena3 | $cadena4: $fichero <br>";
  		print  "OK- No es permitido el acceso ssh al usuario root <br><br>";
	}

}# Fin checkNoRootSSH 





sub checkDisabledCtrlAltDel {

	print "<a name=compruebaShells></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Chequeo inhabilitacion del reseteo de la maquina mediante ctrl+alt+del: </FONT>/etc/ssh/sshd_config";
	print "<br><br>";
	

	my $fichero="/etc/inittab";
	my $cadena="#ca::ctrlaltdel";
	@cadenasabuscar=("$cadena");

	$verifica=0;

	if (!open(TEXTFILE, $fichero)) {
    	print "No se puede abrir el fichero $fichero <br>";
	}

 	while (<TEXTFILE>) {
   	 foreach $word (@cadenasabuscar) {
      	if (index($_, $word) >= 0) {
        	$verifica=$verifica+1;
      	}
    	}
  	}

	if ($verifica>0) {
  		print "OK- El siguiente fichero contiene la cadena $cadena: $fichero <br>";
  		print "OK- No es permitido el renicio e la maquina mediante ctrl+alt+del <br><br>";
	}
	else{
  		print "<FONT COLOR=$color_peligro>PELIGRO: El siguiente fichero NO contiene la cadena $cadena: $fichero <br><br></FONT>";
  		print "<FONT COLOR=$color_peligro>Esta permitido el reinicio de la maquina mediante ctrl+alt+del <br></FONT>";
  		print "<FONT COLOR=$color_peligro>Para deshabilitarlo es necesario comentar la linea ca::ctrlaltdel:/sbin/shutdown -t3 -r now <br></FONT>";
	}


}# Fin checkDisabledCtrlAltDel




sub checkCrontab {

	print "<a name=checkCrontab></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Chequeo inhabilitacion del uso de crontab a todos los usuarios: </FONT>/etc/cron.allow";
	print "<br><br>";
	
	my $fichero="/etc/cron.allow";
	my $cadena="root";
	@cadenasabuscar=("$cadena");

	$verifica=0;

	if (!open(TEXTFILE, $fichero)) {
   	 print "No se puede abrir o no existe el fichero $fichero, Esta permitido el uso del crontab a cualquier usuario  !!!! <br><br>";
	}

 	while (<TEXTFILE>) {
   	 foreach $word (@cadenasabuscar) {
      	if (index($_, $word) >= 0) {
        		$verifica=$verifica+1;
	      }
   	 }
  	}

	if ($verifica>0) {
  		print "OK- El siguiente fichero contiene la cadena $cadena: $fichero <br>";
  		print "OK- limitado el uso del crontab al usuario root <br><br>";
	}
	else{
  		print "<FONT COLOR=$color_peligro>PELIGRO: El siguiente fichero NO contiene la cadena $cadena: $fichero <br><br></FONT>";
  		print "<FONT COLOR=$color_peligro>Esta permitido el uso del crontab a cualquier usuario  !!!!<br></FONT>";
  		print "<FONT COLOR=$color_peligro>Para deshabilitarlo necesario añadir el usuario root al fichero $fichero<br></FONT>";
	}


}# Fin checkCrontab




sub checkApache {


	print "<a name=checkApache></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Chequeo de la configuracion de apache: </FONT>";
	print "<br><br>";
	
	my $apache1="/etc/httpd/conf/httpd.conf";
	my $apache2="/etc/apache2/apache2.conf";
	my $apache3="/etc/apache/apache.conf";

	
   if (open(TEXTFILE, $apache1)) {
   	print "<FONT COLOR=$color_cabecera>Apache RedHat: </FONT> /etc/httpd/conf/httpd.conf <br><br>";
   	while (<TEXTFILE>) {
   		print "$_<br>";
  		}
   }
   elsif (open(TEXTFILE, $apache2)) {
   		print "<FONT COLOR=$color_cabecera>Apache 2 Debian: </FONT> /etc/apache2/apache2.conf <br><br>";
   		while (<TEXTFILE>) {
   			print "$_<br>";
  			}
   	}
   
	elsif (open(TEXTFILE, $apache3)) {
			print "<FONT COLOR=$color_cabecera>Apache 1 Debian: </FONT> /etc/apache/apache.conf <br><br>";
  			while (<TEXTFILE>) {
  				print "$_<br>";
  			}
   	}
   
   else {
   	print "<FONT COLOR=$color_peligro>Ningun apache instalado </FONT><br><br>";
   }
   


}# Fin checkApache






sub recomendaciones {


	print "<a name=recomendaciones></a>";
	print "<br><br><br>";
	print "<FONT COLOR=$color_cabecera>Recomendaciones uso software de seguridad: </FONT>";
	print "<br><br>";
	

	
   print "Como recomendacion podemos hacer uso de las siguientes aplicaciones para comprobar el estado de nuestro sistema<br><br>";


   print "<FONT COLOR=$color_cabecera>- chkrootkit: shell script that checks system binaries for rootkit modification</FONT> http://www.chkrootkit.org/<br>";
   print "<FONT COLOR=$color_cabecera>- AIDE (Advanced Intrusion Detection Environment) </FONT>http://www.cs.tut.fi/~rammer/aide.html<br>";
   print "<FONT COLOR=$color_cabecera>- John the Ripper is a fast password cracker </FONT>http://www.openwall.com/john/ <br>";
   print "<FONT COLOR=$color_cabecera>- Logcheck is a simple utility which is designed to allow a system administrator to view the logfiles which are produced upon hosts under their control. </FONT>http://logcheck.org/ <br>";
   print "<FONT COLOR=$color_cabecera>- Portsentry is an attack detection tool </FONT>http://sourceforge.net/projects/sentrytools/<br>";
   print "<FONT COLOR=$color_cabecera>- HostSentry is a host based intrusion detection tool </FONT><br>";
   print "<FONT COLOR=$color_cabecera>- DenyHosts is a script intended to be run by Linux system administrators to help thwart SSH server attacks  </FONT>http://denyhosts.sourceforge.net/<br>";


}# Fin recomendaciones seguridad

