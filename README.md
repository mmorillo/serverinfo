# Welcome

CGI Perl that displays system (unix) information using a web browser

#Download

Try this link [ServerInfo](https://bitbucket.org/mmorillo/serverinfo/get/master.zip)

The git repository, which means you can clone it, edit it locally/offline, add images or any other file type, and push it back to us. It will be live immediately.

Go ahead and try:

```
$ git clone https://bitbucket.org/mmorillo/serverinfo.git
```


# Install Instructions

```
1- Copy the cgi file to your cgi-bin/ 
2- You can now execute the file in your browser: http://SERVER/cgi-bin/Server-Info.cgi

In Debian you can copy the file in /usr/lib/cgi-bin
```


Have fun!
